# Software Packages

## Apache Foundation Umbrella

### Kafka

Resources:

* https://kafka.apache.org/
* https://en.wikipedia.org/wiki/Apache_Kafka

Notes:

* Apache Kafka is an open-source stream-processing software platform developed by the Apache Software Foundation, written in Scala and Java.
* The project aims to provide a unified, high-throughput, low-latency platform for handling real-time data feeds.
* Has many connectors to injest and disseminate messages

## Apigee

* Build API proxies
* Analysis for API usage
* User console - limit usage by group/API key
* Can do API transformation, like multiple authentication providers, JSON <-> XML
