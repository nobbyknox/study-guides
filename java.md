# Java

## Introduction

Brief notes on stuff that I need to know

## Data Structures

### HashMap

`HashMap` superceeds the old `HashTable`. It provides a one-way mapping from one set of object references to another. Null keys and values are permitted.

### HashSet

`HashSet` implements the `Set` interface and is backed by a `HashMap` instance. No guarantees as to order of set. Constant time performance for basic operations (add, remove, contains, size).

### Array List

Resizable-array implementation of the `List` interface. Allows all elements, including null. Roughly equivalent to `Vector`. Not synchronized.

### Vector

The `Vector` class implements a growable array of objects. Synchronized. Legacy class.

### Linked List

TODO: Write me
