# Design Patterns

## Adapter Pattern

From https://en.wikipedia.org/wiki/Adapter_pattern

> Adapter pattern is a software design pattern (also known as wrapper, an alternative naming shared with the decorator pattern) that allows the interface of an existing class to be used as another interface.[1] It is often used to make existing classes work with others without modifying their source code.
>
> Problems solved:
> 
> * How can a class be reused that does not have an interface that a client requires?
> * How can classes that have incompatible interfaces work together?
> * How can an alternative interface be provided for a class?
> 
> Often an (already existing) class can't be reused only because its interface doesn't conform to the interface clients require.
> 
> The adapter design pattern describes how to solve such problems:
> 
> * Define a separate adapter class that converts the (incompatible) interface of a class (adaptee) into another interface (target) clients require.
> * Work through an adapter to work with (reuse) classes that do not have the required interface.
> 
> The key idea in this pattern is to work through a separate adapter that adapts the interface of an (already existing) class without changing it.
> 
> Clients don't know whether they work with a target class directly or through an adapter with a class that does not have the target interface. 

## Builder Pattern

From https://en.wikipedia.org/wiki/Builder_pattern

> The Builder is a design pattern designed to provide a flexible solution to various object creation problems in object-oriented programming. The intent of the Builder design pattern is to separate the construction of a complex object from its representation.
>
> The Builder design pattern solves problems like:
>
> * How can a class (the same construction process) create different representations of a complex object?
> * How can a class that includes creating a complex object be simplified?
> 
> Creating and assembling the parts of a complex object directly within a class is inflexible. It commits the class to creating a particular representation of the complex object and makes it impossible to change the representation later independently from (without having to change) the class.
> 
> The Builder design pattern describes how to solve such problems:
> 
> * Encapsulate creating and assembling the parts of a complex object in a separate Builder object.
> * A class delegates object creation to a Builder object instead of creating the objects directly.
>
> A class (the same construction process) can delegate to different Builder objects to create different representations of a complex object.

## Chain of Responsibility Pattern

## Proxy Pattern

## Strategy Pattern

From https://en.wikipedia.org/wiki/Strategy_pattern:

> Strategy pattern (also known as the policy pattern) is a behavioral software design pattern that enables selecting an algorithm at runtime. Instead of implementing a single algorithm directly, code receives run-time instructions as to which in a family of algorithms to use.
> 
> Strategy lets the algorithm vary independently from clients that use it. Deferring the decision about which algorithm to use until runtime allows the calling code to be more flexible and reusable.
> 
> For instance, a class that performs validation on incoming data may use the strategy pattern to select a validation algorithm depending on the type of data, the source of the data, user choice, or other discriminating factors. These factors are not known until run-time and may require radically different validation to be performed. The validation algorithms (strategies), encapsulated separately from the validating object, may be used by other validating objects in different areas of the system (or even different systems) without code duplication. 

Can be summarized as coding against an interface with concrete classes providing the actual algorithm.
